print 'Loading...'
import time
import os
import serial
import mido
import sys
from ctypes import *
from mido import Message
from mido.sockets import connect
#TODO Prettify virtual midi port
if os.name == 'nt':
    os.system("start Virtual_MIDI.exe")
time.sleep(2)
try:
    output = connect('localhost', 19104)
except:
    print "We cannot connect to Virtual MIDI port"
    exit()
print "Connected!"

#Send a MIDI Panic, resetting all channels
output.send(mido.parse([176, 123, 0]))

#Open serial port(s)
serials = []
baudrate = 9600
if len(sys.argv) == 1:
    comport = 14
    try:
        ser[0] = serial.Serial('COM'+str(comport), 9600)
        if(ser[0].isOpen()):
            ser[0].close()
        ser[0].open()
        print 'Connected to COM14 at baud 9600'
    except:
        print 'Cannot connect to COM'+str(comport)+' at baud 9600'
else:
    index = 0
    for i in sys.argv:
        if(index > 0):
            if type(i) == type('') and i.find('-b=') > -1: #Is a string AND is our baud setter
                baudrate = i[3:] #Strip out baudrate and display it
            else: #is a comport
                comport = i
                try: 
                    #print 'Try ser['+str(index-1)+'] = serial.Serial(COM'+str(comport)+', '+str(baudrate)+')'
                    serials.append(serial.Serial('COM'+str(comport), baudrate)) #In this way we can have several unique COM ports
                    if(serials  [index-1].isOpen()):
                        serials [index-1].close()
                    serials [index-1].open()
                    print 'Connected to COM'+str(comport)+' at baud '+str(baudrate)
                except Exception:
                    #print Exception
                    #print sys.exc_info()[0]
                    print 'Cannot connect to COM'+str(comport)+' at baud '+str(baudrate)
        index += 1
try:
    while(1):
        for serial in serials:
            if serial.in_waiting >= 3:
                s = serial.read(3)
                print 'Read '+s+' / '+str(serial.in_waiting)
                p = mido.parse([ord(s[0:1])+128, ord(s[1:2]), ord(s[2:3])])
                print p
                output.send(p)
except KeyboardInterrupt:
    print 'Keyboard interrupt'
    for serial in serials:
        serial.close()
except Exception as inst:
    print 'Cannot find COM'+str(comport)
    print inst
#    ser.close()
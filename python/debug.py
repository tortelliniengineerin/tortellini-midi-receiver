print "Loading..."
import time
import mido
import os
import sys
import serial
from ctypes import *

print "Ready!"

#Open a serial port
if len(sys.argv) == 1:
    comport = 14
else:
    comport = sys.argv[1]
if len(sys.argv) <= 2:
    baudrate = 9600
else:
    baudrate = sys.argv[2]
    
try:
    ser = serial.Serial('COM'+str(comport), baudrate)
    if(ser.isOpen()):
        ser.close()
    ser.open()
    while(1):
        if ser.in_waiting:
            s = ser.read(1)
            print 'Read '+s+' ('+str(ord(s))+')'
    #        print c_char(s)
    #        print ord(s) 
            print '     <<'+str(ser.in_waiting)
        
except KeyboardInterrupt:
    print 'Keyboard interrupt'
    ser.close()
except Exception as inst:
    print 'Cannot find COM'+str(comport)
    print inst
#    ser.close()
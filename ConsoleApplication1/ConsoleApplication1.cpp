// ConsoleApplication1.cpp : Defines the entry point for the console application.
//
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>

#include "stdafx.h"
#include "teVirtualMIDI.h"
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <string>
#include "format.h"
#include <iostream>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "19104"

int fullyConnected = 0;

using namespace std;

#define MAX_SYSEX_BUFFER	65535

char *binToStr(const unsigned char *data, DWORD length) {
	static char dumpBuffer[MAX_SYSEX_BUFFER * 3];
	DWORD index = 0;

	while (length--) {
		sprintf(dumpBuffer + index, "%02x", *data);
		if (length) {
			strcat(dumpBuffer, ":");
		}
		index += 3;
		data++;
	}
	return dumpBuffer;
}

void CALLBACK teVMCallback(LPVM_MIDI_PORT midiPort, LPBYTE midiDataBytes, DWORD length, DWORD_PTR dwCallbackInstance) {
	if ((NULL == midiDataBytes) || (0 == length)) {
		printf("empty command - driver was probably shut down!\n");
		return;
	}
	if (!virtualMIDISendData(midiPort, midiDataBytes, length)) {
		printf("error sending data: %d\n" + GetLastError());
		return;
	}

	printf("command: %s\n", binToStr(midiDataBytes, length));
}
int main()
{
	cout << "Initialized\n";
	///////////////////////////////////////////////////////////////////////////
	// MIDI ///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	LPVM_MIDI_PORT port;
	virtualMIDILogging(TE_VM_LOGGING_MISC | TE_VM_LOGGING_RX | TE_VM_LOGGING_TX);

	int pid = GetCurrentProcessId();

	//LPCWSTR portname = fmt::format(L"TCP{}", pid).c_str();
	LPCWSTR portname = L"Tortellini MIDI";
	port = virtualMIDICreatePortEx2(portname, teVMCallback, 0, MAX_SYSEX_BUFFER, TE_VM_FLAGS_PARSE_RX);
	if (!port) {
		printf("could not create port: %d\n", GetLastError());
		return -1;
	}
	else {
		cout << "Created virtual MIDI port ";
		cout << portname;
		cout << "\n";
	}
	///////////////////////////////////////////////////////////////////////////
	// NETWORKING /////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	WSADATA wsaData;
	int iResult;

	SOCKET ListenSocket = INVALID_SOCKET;
	SOCKET ClientSocket = INVALID_SOCKET;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	int iSendResult;
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("Get Address Info failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("Invalid socket found; program failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	// Setup the TCP listening socket
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	freeaddrinfo(result);

	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}


	//////////////////////////////////////////////////////////////////////////////
	// DO STUFF SECTION //////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
	int running = 1;
	while (running) {
		ClientSocket = accept(ListenSocket, NULL, NULL);
		cout << "Connected!" << endl;
		fullyConnected = 1;
		// Receive until the peer shuts down the connection
		do {
			iResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
			if (iResult > 0) {
				printf("Bytes received: %d\n", iResult);
				if (recvbuf[0] == 255) {
					//Kill
					running = 0;
					iResult = 0;
				}

				/////////////////////////////////////////// SEND DATA OVER MIDI
				virtualMIDISendData(port, (LPBYTE)recvbuf, iResult);

			}
			else if (iResult == 0) {
				printf("Connection closing...\n");
				iResult = 0;
				running = 0; //We want the program to close after a closed connection; a new initialization opens a new window
			} else {
				printf("recv failed with error: %d\n", WSAGetLastError());
				closesocket(ClientSocket);
				WSACleanup();
				//End the program
				iResult = 0;
				running = 0;
			}

		} while (iResult > 0);
		
	}
	// shutdown the connection since we're done
	iResult = shutdown(ClientSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		return 1;
	}

	// cleanup
	cout << "Shutting down...";
	closesocket(ClientSocket);
	WSACleanup();
	//_getch();
	closesocket(ListenSocket);
	virtualMIDIShutdown(port);
	virtualMIDIClosePort(port);
    return 0;
}


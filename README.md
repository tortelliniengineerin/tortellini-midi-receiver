# Tortellini MIDI Receiver
The MIDI receiver project allows a device to receive MIDI packets over UART and push them to a virtual MIDI port.

## What's Included?
The project contains the following:

* `python/debug.py` - A debugging script which will read and print out individual characters over UART as it receives them
* `python/receiver.py` - This tool will open a UART connection and read MIDI messages that it sends. Then it passes these messages to a virtual MIDI port which can be read by any digital audio workstation
* `ConsoleApplication1/` - Windows computers cannot directly interface with virtual MIDI ports, so it requires a separate application which is called automatically by the Python script

## Open Specific COM Ports
By default, the script opens `COM14`. If you want it to access a different port, pass that as a parameter when you call the script:

    cd python
    python receiver.py 16
    
By default, the script reads UART commands at `9600`. If you want it to read at a different baud rate, pass that as a flag parameter when you call the script:

    cd python
    python receiver.py -b=19200 16
    
If you want to connect to multiple COM ports at the same time, this is possible by entering more than one port number as a parameter

    cd python
    python receiver.py 14 15 16
    
You can also change the baud rate for each com port you're connecting to

    cd python
    python receiver.py -b=19200 14 -b=115200 15 -b=9600 16

Type `Ctrl + C` to exit.

## Installation
### Python Installs
To get started, you first need to install Python. After that, you need to install some python libraries.  

This can be done by running `pip install -r python/requirements.txt` in a console.

You'll be installing:

* pyserial
* mido

### Windows Installs
Install the LoopMIDI driver that can be found from the . Run `loopMIDISetup.exe` on Windows computers. Mac OSX and Linux machines already have the necessary framework to accept virtual MIDI messages.

If you want to download the SDK, you can grab it here: <a href='http://www.tobias-erichsen.de/software/virtualmidi/virtualmidi-sdk.html'>Virtual MIDI SDK</a>. It isn't necessary unless you want to modify the C++ project.

## Troubleshooting
### `ucrtbase.dll` 
If you're getting this message, then you will need to move the `ucrtbase.dll` file from the `libs\ucrt` directory to `Windows\System32`.

### Issues with printing
If you see error messages regarding printing, you may need to switch from Python 3.X to Python 2.7.

### "Cannot find module ..."
If you are having a message in your command line about modules not being found, you may need to install python libraries (see "Python Installs").

### MIDI-Ox finds no Input Devices
The program <a href='http://www.midiox.com/'>Midi-Ox</a> is a great utility tool to test out Midi input. However, if you load the python script and it works correctly, but Midi-Ox says there's no input devices found, you're SOL.